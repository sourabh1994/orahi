package com.example.orahitask.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.orahitask.R;

import java.util.Random;


public class LoginFrag extends Fragment {
    Button generateOtpBtn,verifyOtpBtn,resendOtp;
    EditText mobileEt,otpEt;
    View rootView;
    long optNumber;
    LinearLayout otpLy;
    Random rand = new Random();
    private static ProgressDialog dialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView= inflater.inflate(R.layout.fragment_login, container, false);
        mobileEt=rootView.findViewById(R.id.mobileEt);
        otpEt=rootView.findViewById(R.id.verifyOtpEt);
        otpLy=rootView.findViewById(R.id.otpLy);
        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("title");
        dialog.setMessage("message");
        dialog.setCancelable(false);
        generateOtpBtn=rootView.findViewById(R.id.generateOtpBtn);
        verifyOtpBtn=rootView.findViewById(R.id.verifyBtn);
        resendOtp=rootView.findViewById(R.id.resendOtpBtn);
        verifyOtpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(otpEt.getText().toString().trim().equalsIgnoreCase(String.valueOf(optNumber)))
                    Toast.makeText(getActivity(), "Login Success", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getActivity(), "Please enter correct otp", Toast.LENGTH_SHORT).show();
            }
        });
        generateOtpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mobileEt.getText().toString().trim().length()==0){
                    Toast.makeText(getActivity(), "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                    return;
                }
                generateOtp();
            }
        });

        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mobileEt.getText().toString().trim().length()==0){
                    Toast.makeText(getActivity(), "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                    return;
                }
                generateOtp();
            }
        });
        return rootView;
    }
    public void generateOtp(){
        dialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                optNumber=rand.nextInt(1000);
                Toast.makeText(getActivity(), "OTP : "+optNumber, Toast.LENGTH_SHORT).show();
                otpLy.setVisibility(View.VISIBLE);
            }
        }, 5000);
    }
}
