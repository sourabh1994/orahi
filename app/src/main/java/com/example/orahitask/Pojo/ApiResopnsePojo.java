package com.example.orahitask.Pojo;

import java.util.List;

public class ApiResopnsePojo {
    public String status;
    public String  message;
    public List<ApiResponseDataPojo> data;

    public ApiResopnsePojo(String status, String message, List<ApiResponseDataPojo> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ApiResponseDataPojo> getData() {
        return data;
    }

    public void setData(List<ApiResponseDataPojo> data) {
        this.data = data;
    }
}
