package com.example.orahitask.Pojo;

public class ApiResponseDataPojo {
    public String month;
    public String stat;

    public ApiResponseDataPojo(String month) {
        this.month = month;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}
