package com.example.orahitask.Activity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.example.orahitask.Pojo.ApiResopnsePojo;
import com.example.orahitask.Pojo.PointsPojo;
import com.example.orahitask.Retrofit.ApiClient;
import com.example.orahitask.Retrofit.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GraphActivity extends AppCompatActivity {
    ArrayList<PointsPojo> pointsPojosList = new ArrayList<>();
    PointsPojo pointsPojo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getStats();
            }
        }, 8000);

        DemoView demoview = new DemoView(this);
        setContentView(demoview);

    }
    public void getStats(){
        ApiInterface apiInterface = ApiClient.getClientNew("").create(ApiInterface.class);
        Call<ApiResopnsePojo> call = apiInterface.getStats();
        call.enqueue(new Callback<ApiResopnsePojo>() {
            @Override
            public void onResponse(Call<ApiResopnsePojo> call, Response<ApiResopnsePojo> response) {
                ApiResopnsePojo apiResopnsePojo=response.body();
                if (response.isSuccessful() && apiResopnsePojo.getStatus().toLowerCase().equalsIgnoreCase("true")){
                    for(int i=0;i<apiResopnsePojo.getData().size();i++){
                        pointsPojo = new PointsPojo(i*100, Integer.parseInt(apiResopnsePojo.getData().get(i).getStat()));
                        pointsPojosList.add(pointsPojo);
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResopnsePojo> call, Throwable t) {

            }
        });
    }

    private class DemoView extends View {
        private Paint paint;
        Context app_context;

        public DemoView(Context context) {
            super(context);
            paint = new Paint();
            paint.setColor(Color.GRAY);
            app_context = context;
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            int width = 1000;
            int height = 1200;
            setMeasuredDimension(width, height);
        }

        @Override
        public void onDraw(Canvas canvas) {
            canvas.drawColor(Color.BLUE);
            paint.setColor(Color.GRAY);
            paint.setTextSize(50);
            ArrayList<Integer> xStopPointsLine1=new ArrayList<>();
            ArrayList<Integer> yStopPointsLine1=new ArrayList<>();
            for(int i =0;i<pointsPojosList.size();i++){
                xStopPointsLine1.add(pointsPojosList.get(i).getX());
                yStopPointsLine1.add(pointsPojosList.get(i).getY());
            }
            xStopPointsLine1.add(1500);
            xStopPointsLine1.add(1500);
            yStopPointsLine1.add(0);
            yStopPointsLine1.add(0);
            for (int i = 0; i < yStopPointsLine1.size(); i++) {
                paint.setColor(Color.GRAY);
                paint.setStrokeWidth(8);
                if (i == 0) {
                    canvas.drawLine(xStopPointsLine1.get(i), yStopPointsLine1.get(i), xStopPointsLine1.get(i + 1), yStopPointsLine1.get(i + 1), paint);
                    paint.setColor(Color.GREEN);
                    paint.setColor(Color.RED);
                    canvas.drawCircle(xStopPointsLine1.get(i), yStopPointsLine1.get(i), 12, paint);
                    invalidate();
                } else if (i > 0 && i < yStopPointsLine1.size() - 1) {
                    canvas.drawLine(xStopPointsLine1.get(i), yStopPointsLine1.get(i), xStopPointsLine1.get(i+1), yStopPointsLine1.get(i+1), paint);
                    paint.setColor(Color.RED);
                    postInvalidateDelayed(1500);
                    canvas.drawCircle(xStopPointsLine1.get(i), yStopPointsLine1.get(i), 12, paint);
                } else if (i == yStopPointsLine1.size() - 1) {
                    paint.setColor(Color.RED);
                    canvas.drawCircle(xStopPointsLine1.get(i), yStopPointsLine1.get(i), 12, paint);
                }
            }
        }
    }
}
