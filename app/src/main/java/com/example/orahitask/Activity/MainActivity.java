package com.example.orahitask.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.orahitask.Fragment.LoginFrag;
import com.example.orahitask.R;

public class MainActivity extends AppCompatActivity {
    FragmentManager fm;
    Button loginBtn,graphBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fm=getSupportFragmentManager();
        loginBtn=findViewById(R.id.loginBtn);
        graphBtn=findViewById(R.id.graphBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchFragment(new LoginFrag(),0,"Login");
            }
        });
        graphBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, GraphActivity.class));

                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_left);
            }
        });

    }
    public void switchFragment(Fragment fragment, int position, String fragName) {
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        if (position == 0) {
            fragmentTransaction.add(R.id.container, fragment, fragName);
        } else if (position == 2) {
            fragmentTransaction.replace(R.id.container, fragment, fragName);
        }
        else if (position == 3) {
            fragmentTransaction.replace(R.id.container, fragment, fragName);
        }
        else if (position == 1) {
            fragmentTransaction.replace(R.id.container, fragment, fragName);
            fragmentTransaction.addToBackStack(fragName);
        } else {
            fragmentTransaction.replace(R.id.container, fragment, fragName);
            fragmentTransaction.addToBackStack(fragName);
        }
        fragmentTransaction.commit();
    }

}
