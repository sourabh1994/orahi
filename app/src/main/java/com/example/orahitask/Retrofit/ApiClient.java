package com.example.orahitask.Retrofit;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static final int TIME_OUT_100 = 100;
    public static String tokenR;
    private static Retrofit retrofit = null;
    private static OkHttpClient.Builder client = new OkHttpClient.Builder()
            .connectTimeout(TIME_OUT_100, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT_100, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT_100, TimeUnit.SECONDS);
    public static Retrofit getClientNew(final String token) {
        tokenR=token;
        client.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request;
                if (tokenR.equalsIgnoreCase("")) {
                    request = original.newBuilder()
                            .method(original.method(), original.body())
                            .build();
                } else {
                    request = original.newBuilder()
                            .header("Token", tokenR)
                            .method(original.method(), original.body())
                            .build();
                }

                return chain.proceed(request);
            }
        });

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppUri.baseUrl)
                    .client(client.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
